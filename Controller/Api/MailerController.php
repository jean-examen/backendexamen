<?php

require_once "./Model/MailerModel.php";
require_once "./Validations/Validations.php";
class MailerController extends MailerModel
{
    function mailerAction()
    {
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        $validations = new Validations();

        if (strtoupper($requestMethod) == 'POST') {
            $json = file_get_contents('php://input');
            $data = json_decode($json);

            if ($validations->validateEmail($data->receiver)[0]) {
                $this->sendMail($data->receiver, $data->message, $data->subject);
            } else {
                echo json_encode($validations->validateEmail($data->receiver)[1]);
            }
        }
    }
}
