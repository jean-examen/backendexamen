<?php
class Procedure
{
    public static $sp_user_list = "call sp_users_getAll()";

    //------------------------ PATIENT PROCEDURES -----------------------

    public static $sp_patients_create = "call sp_patients_create(?,?,?,?,?,?,?,?)";
    public static $sp_patients2_create = "call sp_patients2_create(?,?,?,?,?,?,?,?)";
    public static $sp_patients_getAll = "CALL sp_patients_getAll()";
    public static $sp_patients_getByLimit = "call sp_patients_getbylimit(?)";
    public static $sp_patients_update = "call sp_patients_update(?,?,?,?,?,?,?,?,?)";
    public static $sp_patients_search = "call sp_patients_search(?)";
    public static $sp_patients_status = "call sp_patients_status(?)";
    public static $sp_patient_findById = "call sp_patients_findbyid(?)";
    public static $sp_patients_exist = "call sp_patients_exist(?)";
    public static $sp_patients_getByOption = "call sp_patients_getByOption(?)";
    public static $sp_patient_validate_id_card = "call sp_patient_validate_id_card(?)";
    public static $sp_patient_validate_email = "call sp_patient_validate_email(?)";

    //------------------------ APPOINMENTS PROCEDURES -----------------------
    public static $sp_appointments_create = "call sp_appointments_create(?,?,?)";
    public static $sp_appointments_delete = "call sp_appointments_delete(?)";
    public static $sp_appointments_find_data = "call sp_appointments_find_data(?,?,?)";
    public static $sp_appointments_history = "call sp_appointments_history(?,?)";
    public static $sp_appointments_update = "call sp_appointments_update(?,?,?)";
    public static $sp_appointments_exist = "call sp_appointments_exist(?)";
    public static $sp_appointments_findbyid = "call sp_appointments_findbyid(?)";
    public static $sp_appointments_patient_lastAppointment_bymonth = "call sp_appointments_patient_lastAppointment_bymonth(?)";
    public static $sp_appointments_get_today = "call sp_appointments_get_today()";

    public static $sp_appointment_change_assistance = "call sp_appointment_change_assistance(?)";

    public static $sp_appointments_getNextDay = "call sp_appointments_getNextDay()";


    //------------------------ IMAGES PROCEDURES -----------------------

    public static $sp_images_create = "call sp_images_create(?,?)";
    public static $sp_images_getbyid = "call sp_images_getbyid(?)";
    public static $sp_images_delete = "call sp_images_delete(?)";
    public static $sp_images_exist = "call sp_images_exist(?)";

    //---------------------------- TRATAMIENTOS ---------------------- 

    public static $sp_treatments_insert = "call sp_treatments_insert(?,?,?)";
    public static $sp_treatments_delete = "call sp_treatments_delete(?)";
    public static $sp_treatments_update = "call sp_treatments_update(?,?,?,?)";
    public static $sp_treatments_select = "call sp_treatments_select()";
    public static $sp_treatments_getbyid = "call sp_treatments_getbyid(?)";
    public static $sp_treatments_exist = "call sp_treatments_exist(?)";


}


